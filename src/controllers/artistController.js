const express = require('express');

const Artist = require('../modules/Artist');

const router = express.Router();

router.get("/", async (request, response)=> {
    var artists = await Artist.find()
    return response.json({artists});
})

router.post('/register', async(request, response)=>{
    try{
        const artist = await Artist.create(request.body);
        return response.send({artist});
    }
    catch (err) {
        return response.status(400).send({error: 'Registration failed'})
    }
})

module.exports = app => app.use('/artist', router);