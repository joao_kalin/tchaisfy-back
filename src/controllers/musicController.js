const express = require('express');

const Music = require('../modules/music');

const router = express.Router();

router.post("/register", async (request, response)=>{
    try{
        const music = await Music.create(request.body);
        return response.send({music});
    }
    catch (err) {
        return response.status(400).send({error: 'Registration failed'})
    }
})


router.get("/", async (request, response)=> {
    var musics = await Music.find()
    musics.forEach(function(item){
        item.url = "";
    })
    return response.json({musics});
})


router.get("/:id", async (request, response) => {
    const{id} = request.params;
    const music = await Music.findOne({_id: id});
    if (music){
        music.entry += 1;
        return response.json(music)
    }
    return response.status(404).send();
})


module.exports = app => app.use('/music', router);
