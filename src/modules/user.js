const mongoose = require('../database/index.js');

const UserSchema = new mongoose.Schema({
    name:{
        type: String,
        require: true,
    },
    email: {
        type: String,
        unique: true,
        required: true,
        lowercase: true,
    },
    password:{
        type: String,
        required: true,
        select: false,
    },
    cratedAt:{
        type: Date,
        default: Date.now,
    },
})
module.exports = mongoose.model('User', UserSchema);