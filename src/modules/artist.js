const mongoose = require('../database/index.js');

const ArtistSchema = new mongoose.Schema({
    name:{
        type: String,
        require: true,
    },
    style: {
        type: String,
        required: true,
    },
    image:{
        type: String,
        required: true,
    },
    cratedAt:{
        type: Date,
        default: Date.now,
    },
})
module.exports = mongoose.model('Artist', ArtistSchema);