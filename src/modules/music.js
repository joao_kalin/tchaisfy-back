const mongoose = require('../database/index.js');

const MusicSchema = new mongoose.Schema({
    name:{
        type: String,
        require: true,
    },
    artist:{
        type: String,
        require: true,
    },
    url: {
        type: String,
        required: true,
    },
    image:{
        type: String,
        required: true,
    },
    entry:{
        type: Number,
    },
    cratedAt:{
        type: Date,
        default: Date.now,
    },
})
module.exports = mongoose.model('Music', MusicSchema);