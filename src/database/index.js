const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost/backzada')
mongoose.Promise = global.Promise;

module.exports = mongoose;