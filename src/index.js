const { response } = require('express');
const express = require('express');
const bodyParser = require('body-parser')
const {v4 :uuidv4} = require('uuid')
const app = express();


app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));
app.use(bodyParser.urlencoded({extended: false}))

app.listen(3333, () => {
    console.log(`O servidor está rodando na porta 3333`)
  })

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

require('./controllers/authController')(app);
require('./controllers/musicController')(app);
require('./controllers/artistController')(app);

const artistas = [];
app.use(express.json())



